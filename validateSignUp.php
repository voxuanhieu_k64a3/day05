<?php
session_start();
$img = $_SESSION["uploadfile"];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/css/bootstrap-responsive.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.js"></script>
    <style>
        .error {
            color: red;
            font-size: 12px;

        }

        .note {
            color: red;
        }

        #blah {
            width: 100px;
            height: 60px;
        }
    </style>
</head>

<body>
    <div class="center">
        <div class="login">
            <div class="login_wrap">
                <form method="POST" enctype="multipart/form-data">
                    <?php echo "<div class='cssLogin'>
                        <label class='loginInput' >Họ và tên  <span class='note'>*</span> </label>
                        <p?>{$_SESSION["fullName"]} </p>
                    </div>";
                    ?>


                    <?php

                    echo "<label class='gender'> Giới tính   <span class='note'>*</span></label>";

                    ?>
                    <?php echo strcmp($_SESSION['gender'], "{Nam}") == 0  ? "Nam" : "Nữ" ?>

                    <div class='selectValue'>
                        <label class='divideValues'> Phân Khoa <span class='note'>*</span></label>

                        <?php echo $_SESSION['department'] == "MAT" ? 'Khoa học máy tính' : 'Khoa học vật liệu'  ?>


                    </div>

                    <div class='selectValue d-flex'>
                        <label class='divideValue'> Ngày sinh <span class='note'>*</span></label>
                        <div class="input-group input-daterange">
                            <?php echo $_SESSION['birthday'] ?>
                        </div>
                    </div>


                    <div class='cssLogin'>
                        <div class='loginInput' style="height:35px; width:40%"> Địa chỉ </div>
                        <div style="height:35px; width:60%;font-size:12px"><?php echo $_SESSION['address'] ?></div>
                    </div>


                    <div class='cssLogin'>
                        <div class='loginInput' style="height: 35px;">Hình ảnh</div>
                        <div>
                            <img id='blah' src='<?php echo $img ?>' />
                        </div>
                    </div>




                    <?php echo "<div class='push'><button class='submit' type='submit' name='submit'> Xác nhận</button></div>" ?>

                </form>

            </div>


        </div>
    </div>
</body>

</html>
<script>
    $(document).ready(function() {

        $('.input-daterange').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            calendarWeeks: true,
            clearBtn: true,
            disableTouchKeyboard: true
        });

    });
</script>
<?php
?>