<?php
$err = array();
function isDate($string)
{
    if (preg_match('/^([0-9]{1,2})\\-([0-9]{1,2})\\-([0-9]{4})$/', $string)) {
        return true;
    } else {
        return false;
    }
}
if (isset($_POST['submit'])) {

    error_reporting(E_ALL);
    // var_dump($_POST['gender']);
    session_start();
    if (empty($_POST['fullName'])) {
        $err['name'] = "Hãy nhập tên";
    } else {
        $_SESSION['fullName'] = $_POST['fullName'];
    }
    if (empty($_POST['gender'])) {
        $err['gender'] = "Hãy chọn giới tính";
    } else {
        $_SESSION['gender'] = $_POST['gender'];
    }
    if (empty($_POST['department'])) {
        $err['department'] = "Hãy chọn phân khoa";
    } else {
        $_SESSION['department'] = $_POST['department'];
    }
    if (empty($_POST['birthday'])) {
        $err['birthday'] = "Hãy nhập ngày sinh";
    } else if (!empty($_POST['birthday'])) {
        if (!isDate($_POST['birthday'])) {
            $err['birthday'] = "Hãy nhập đúng định dạng";
        } else {
            $_SESSION['birthday'] = $_POST['birthday'];
        }
    }
    if (!empty($_FILES['uploadfile'])) {
        
        $path = "upload";
        
        if (!is_dir($path)) {
            mkdir("./$path");
        }

        $info = pathinfo($_FILES['uploadfile']['name']);
        $_FILES['uploadfile']['name'] = $info['filename'] . "_" . date("Ymdhis") . "." . $info['extension'];
        $supported = ["jpeg", "png"];
        if (!in_array($info['extension'], $supported)) {
            $err["image"] = "Định dạng image là " . $supported[0] . "," . $supported[1];
        } else {
            move_uploaded_file($_FILES['uploadfile']['tmp_name'], "./$path/" . $_FILES['uploadfile']['name']);
            $_SESSION["uploadfile"] = "./$path/" . $_FILES['uploadfile']['name'];
        }
    }


    if (empty($err)) {
        $newURL = "validateSignUp.php";
        header('Location:' . $newURL);
    }
}
